/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.molaya.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Execute commands
 *
 * @author molaya
 */
public class RuntimeExec {

    Runtime rt;
    StreamWrapper error, output;
    String msg, msgerror;

    public RuntimeExec() {
        this.rt = Runtime.getRuntime();
        this.error = null;
        this.output = null;
        this.msgerror = null;
        this.msg = null;
    }

    /**
     * getStreamWrapper
     *
     * @param is
     * @param type
     * @return
     */
    public StreamWrapper getStreamWrapper(InputStream is, String type) {
        return new StreamWrapper(is, type);
    }

    /**
     * Get Console Ouput Ok message
     *
     * @return
     */
    public String getMessage() {
        return output.getMessage();
    }

    /**
     * Get Console Output Error message
     *
     * @return
     */
    public String getMessageError() {
        return error.getMessage();
    }

    /**
     * Run command on Console, in thread.
     *
     * @param command Command to run in console
     */
    public void runProcess(String command) {
        int exitVal;
        Process proc;

        try {

            proc = rt.exec(command);
            error = this.getStreamWrapper(proc.getErrorStream(), "ERROR");
            output = this.getStreamWrapper(proc.getInputStream(), "OUTPUT");

            //start process
            error.start();
            output.start();
            error.join(3000);
            output.join(3000);

            exitVal = proc.waitFor();
        } catch (IOException  ex) {
            Logger.getLogger(RuntimeExec.class.getName()).log(Level.SEVERE, null, ex);
            output = null;
        } catch (InterruptedException ex){
        	Logger.getLogger(RuntimeExec.class.getName()).log(Level.SEVERE, null, ex);
        	output = null;
        } catch (RuntimeException ex){
        	output = null;
        }
    }
    
    public void runProcess(String command, boolean debug) {
        int exitVal;
        Process proc;

        try {

            proc = rt.exec(command);
            error = this.getStreamWrapper(proc.getErrorStream(), "ERROR");
            output = this.getStreamWrapper(proc.getInputStream(), "OUTPUT");

            //start process
            error.start();
            output.start();
            error.join(3000);
            output.join(3000);

            exitVal = proc.waitFor();
            if(debug)
            	System.out.println("Process waitFor = " + exitVal);
        } catch (IOException  ex) {
            Logger.getLogger(RuntimeExec.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex){
        	Logger.getLogger(RuntimeExec.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
