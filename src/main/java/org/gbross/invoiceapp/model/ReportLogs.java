package org.gbross.invoiceapp.model;

import java.util.Date;



public class ReportLogs {
	private Date date;
	private String username;
	private String rolename;
	private String module;
	private String status;
	private String detail;
	
	public ReportLogs(){
		
	}
	
	public ReportLogs(Date date, String username, String rolename,
			String module, String status, String detail) {
		this.date = date;
		this.username = username;
		this.rolename = rolename;
		this.module = module;
		this.status = status;
		this.detail = detail;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	
}
