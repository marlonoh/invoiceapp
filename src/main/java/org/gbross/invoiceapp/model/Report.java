package org.gbross.invoiceapp.model;

import java.util.Date;

public class Report {
	private String name;
	private String module;
	private String role;
	private String description;
	private Date date;
	private String time;
	private String status;

	public Report() {

	}

	/**
	 * Constructor with all parameters
	 * @param name
	 * @param module
	 * @param role
	 * @param description
	 * @param date
	 * @param time
	 * @param status
	 */
	public Report(String name, String module, String role, String description,
			Date date, String time, String status) {
		super();
		this.name = name;
		this.module = module;
		this.role = role;
		this.description = description;
		this.date = date;
		this.time = time;
		this.status = status;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
