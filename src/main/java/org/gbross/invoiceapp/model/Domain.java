package org.gbross.invoiceapp.model;

import java.util.ArrayList;
import java.util.List;

import org.gbross.invoiceapp.adtools.AdConnector;
import org.gbross.invoiceapp.adtools.AdConnectorImpl;
import org.gbross.invoiceapp.dao.DomainDAO;
import org.springframework.stereotype.Component;

@Component
public class Domain {
	private String domain;
	private String userAd;
	private String passAd;
	private String ipAd;
	private String upnAd;
	private String ouGroups;
	private String logo;
	private int id;

	/**
	 * Domain Constructor by Default
	 */
	public Domain() {
		this.userAd = "chgpass";
		this.passAd = "Gesti0nCOrb3";
		this.ipAd = "10.201.1.15";
		this.upnAd = "crediorbe.local";
		this.ouGroups = "Perfiles_Nav";
	}

	public Domain(String domain, String userAd, String passAd, String ipAd,
			String upnAd, String ouGroups, String logo, int id) {
		this.domain = domain;
		this.userAd = userAd;
		this.passAd = passAd;
		this.ipAd = ipAd;
		this.upnAd = upnAd;
		this.ouGroups = ouGroups;
		this.logo = logo;
		this.id = id;
	}

	public Domain(String userAd, String passAd, String ipAd, String upnAd) {
		this.userAd = userAd;
		this.passAd = passAd;
		this.ipAd = ipAd;
		this.upnAd = upnAd;
	}
	
	public Domain(String domain, String userAd, String passAd, String ipAd,
			String upnAd, String ouGroups, String logo) {
		super();
		this.domain = domain;
		this.userAd = userAd;
		this.passAd = passAd;
		this.ipAd = ipAd;
		this.upnAd = upnAd;
		this.ouGroups = ouGroups;
		this.logo = logo;
	}
	
	public Domain(String userAd, String passAd, String ipAd, String upnAd, String ouGroups) {
		this.userAd = userAd;
		this.passAd = passAd;
		this.ipAd = ipAd;
		this.upnAd = upnAd;
		this.ouGroups = ouGroups;
	}

	/**
	 * Set domain by some parameters.
	 * 
	 * @param userAd
	 * @param passAd
	 * @param ipAd
	 * @param upnAd
	 */
	public void setDomain(String userAd, String passAd, String ipAd,
			String upnAd) {
		this.userAd = userAd;
		this.passAd = passAd;
		this.ipAd = ipAd;
		this.upnAd = upnAd;
	}
	
	
	
	/**
	 * Set domain using all parameters.
	 * @param userAd
	 * @param passAd
	 * @param ipAd
	 * @param upnAd
	 * @param ouGroups
	 */
	public void setDomain(String userAd, String passAd, String ipAd,
			String upnAd, String ouGroups) {
		this.userAd = userAd;
		this.passAd = passAd;
		this.ipAd = ipAd;
		this.upnAd = upnAd;
		this.ouGroups = ouGroups;
	}
	
	public void setDomain(String userAd, String passAd, String ipAd,
			String upnAd, String ouGroups, String logo) {
		this.userAd = userAd;
		this.passAd = passAd;
		this.ipAd = ipAd;
		this.upnAd = upnAd;
		this.ouGroups = ouGroups;
		this.logo = logo;
		this.id = id;
	}
	
	/**
	 * Set domain by name, from DataBases
	 * 
	 * @param name
	 * @param dao
	 */
	public void setDomain(String name, DomainDAO dao) {
		setDomain(dao.getDomain(name));
		System.out.println("Setted domain: "+this.upnAd);
	}
	
	/**
	 * Set domain by Domain Object
	 * 
	 * @param dom
	 */
	public void setDomain(Domain dom) {
		this.ipAd = dom.getIpAd();
		this.ouGroups = dom.getOuGroups();
		this.passAd = dom.getPassAd();
		this.upnAd = dom.getUpnAd();
		this.userAd = dom.getUserAd();
		this.logo = dom.getLogo();
		this.id =dom.getId();
		this.domain = dom.getDomain();
	}
	
	/**
	 * Get List Groups From AD.
	 * 
	 * @param ad
	 * @return
	 */
	public List<String> getADGroups(AdConnector ad) {
		List<String> groups = new ArrayList<String>();
		groups = ad.getAllNavigationGroups(this.ouGroups);
		return groups;
	}
	
	/**
	 * Get Name of active Domain.
	 * 
	 * @return
	 */
	public String getActiveDomainName(){
		return this.upnAd;
	}
	
	/**
	 * Get Domain Object of active Domain
	 * 
	 * @return
	 */
	public Domain getActiveDomain(){
		return this;
	}

	public String getOuGroups() {
		return ouGroups;
	}

	public void setOuGroups(String ouGroups) {
		this.ouGroups = ouGroups;
	}

	public String getUserAd() {
		return userAd;
	}

	public void setUserAd(String userAd) {
		this.userAd = userAd;
	}

	public String getPassAd() {
		return passAd;
	}

	public void setPassAd(String passAd) {
		this.passAd = passAd;
	}

	public String getIpAd() {
		return ipAd;
	}

	public void setIpAd(String ipAd) {
		this.ipAd = ipAd;
	}

	public String getUpnAd() {
		return upnAd;
	}

	public void setUpnAd(String upnAd) {
		this.upnAd = upnAd;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
