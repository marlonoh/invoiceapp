package org.gbross.invoiceapp.model;

import java.util.HashMap;
import java.util.List;

import org.gbross.invoiceapp.util.UserAdConnector;
import org.springframework.stereotype.Component;

@Component
public class HistoryMemory {
	private HashMap<String, List<String>> groupMap;
	private HashMap<String, UserAdConnector> userMap;
	private HashMap<String, Integer> statusMap;
	private HashMap<String, List<DsnObjectAd>> dsnMap;
	
	public HistoryMemory(){
		groupMap = new HashMap<String, List<String>>();
		userMap = new HashMap<String, UserAdConnector>();
		statusMap = new HashMap<String, Integer>();
		dsnMap = new HashMap<String, List<DsnObjectAd>>();
	}
	
	public UserAdConnector getUserMap(String key){
		return userMap.get(key);
	}
	
	public List<DsnObjectAd> getDsnMap(String key){
		return dsnMap.get(key);
	}
	
	public List<String> getGroupMap(String key){
		return groupMap.get(key);
	}
	
	public boolean getStatusMap(String key){
		if (statusMap.get(key) == null)
			return false;
		return true;
	}
	
	public void setUserMap(String key, UserAdConnector value){
		userMap.put(key, value);
	}
	
	public void setDsnMap(String key, List<DsnObjectAd> value){
		dsnMap.put(key, value);
	}
	
	
	public void setGroupMap(String key, List<String> value){
		groupMap.put(key, value);
		if(value.size()<1){
			statusMap.put(key, 500);
		}
		statusMap.put(key, 200);
	}
	
}
