package org.gbross.invoiceapp.model;

import java.util.List;

import org.gbross.invoiceapp.adtools.AdConnector;
import org.gbross.invoiceapp.adtools.AdConnectorImpl;
import org.gbross.invoiceapp.util.StdOutHandle;

/**
 * @author molaya
 *
 */
public class UserApp {
	private String username;
	private String password;
	private String name;
	private String email;
	private int status;
	private int roleid;
	private int id;
	private String role;
	private Domain domain;
	private String mydomain;
	

	public UserApp() {

	}

	public UserApp(String username, String password, String name, String email,
			int status, int roleid, int id, String role, Domain domain) {
		this.username = username;
		this.password = password;
		this.name = name;
		this.email = email;
		this.status = status;
		this.roleid = roleid;
		this.id = id;
		this.role = role;
		this.domain = domain;
		this.mydomain = domain.getDomain();
	}

	public UserApp(String username, String password, String name, String email,
			int roleid, int id, int status, String role) {
		this.username = username;
		this.password = password;
		this.name = name;
		this.email = email;
		this.roleid = roleid;
		this.role = role;
		this.id = id;
		this.status = status;
		this.domain = null;
		this.mydomain = "";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getRoleid() {
		return roleid;
	}

	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;

	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getMydomain() {
		return mydomain;
	}

	public void setMydomain(String mydomain) {
		this.mydomain = mydomain;
	}

	public Domain getDomain() {
		return this.domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public String getDomainName() {
		return this.domain.getDomain();
	}

	@Override
	public String toString() {
		return "UserApp [username=" + username + ", password=" + password
				+ ", name=" + name + ", email=" + email + ", status=" + status
				+ ", roleid=" + roleid + ", id=" + id + ", role=" + role
				+ ", domain=" + domain.getDomain() + "]";
	}


	
}
