package org.gbross.invoiceapp.adtools;

import java.util.List;

import org.gbross.invoiceapp.model.Domain;
import org.gbross.invoiceapp.model.DsnObjectAd;

public interface AdConnector {

	
	/**
	 * Set new password Active Directory
	 *
	 * @param pwdad
	 */
	public abstract void setPassUsrAd(String pwdad);

	/**
	 * Get String ObjectDSN
	 *
	 * @return
	 */
	public abstract String getObjectString();

	/**
	 * Return List with all group names based in OU parameter.
	 * 
	 * @param ou Organization unit on AD where you find the groups.
	 */
	public abstract List<String> getAllNavigationGroups(String ou);
	/**
	 * Get String Errors
	 *
	 * @return
	 */
	public abstract String getErrors();
	
	public abstract void setDomain(Domain domain);

	/**
	 * Set User with Administrator permissions to Send commands
	 *
	 * @param usrad
	 */
	public abstract void setAdminUsrAd(String usrad);

	/**
	 * Get DSN of users, computers, groups from AD.
	 *
	 * @param name
	 *            Name, alias or samid of object
	 * @param type
	 *            [user/computer/group]
	 * @return [true/false]
	 */
	public abstract boolean getObjectFromAD(String name, String type);

	/**
	 * Get Group of ObjectDSN
	 *
	 * @param name
	 * @param type
	 * @param isDsn
	 * @return
	 */
	public abstract String getGroupOfObjectDSN(String name, String type,
			boolean isDsn);

	/**
	 * Remove Group and Add to New Group
	 *
	 * @param name
	 * @param type
	 * @param isDsn
	 * @return
	 */
	public abstract boolean changeObjectGroup(String newGroup, String name,
			String type, boolean isDsn);

	/**
	 * Add Members to a Specific Group
	 *
	 * @param group
	 *            Name of Group
	 * @param name
	 *            Name of Member; could be a DSN name
	 * @param type
	 *            Type [user/computer]
	 * @param isDsn
	 *            [true/false]
	 * @return [true/false] Added or not
	 */
	public abstract boolean addMembersToGroup(String group, String name,
			String type, boolean isDsn);

	public abstract boolean deleteMembersFromGroup(String group, String name,
			String type, boolean isDsn);

	/**
	 * Return the DSN Name
	 *
	 * @param user
	 * @return
	 */
	public abstract String getUserDSN(String user);

	/**
	 * Return the DSN Name
	 *
	 * @param user
	 * @return
	 */
	public abstract String getObjectDSN(String name, String type);

	/**
	 * Reset password of Active Directory Users by Userdsn
	 *
	 * @param user
	 *            Must be the DSN name
	 * @param pass
	 * @return [TRUE,FALSE]
	 */
	public abstract boolean resetPasswordUser(String user, String pass);

	/**
	 * Reset password of Active Directory Users by username
	 *
	 * @param user
	 *            Must be te DSN name
	 * @param pass
	 * @return [TRUE,FALSE]
	 */
	public abstract boolean resetPasswordUsername(String user, String pass);

	/**
	 * Add users to Active Directory
	 * @param user
	 * @param pass
	 * @param samid
	 * @param office
	 * @param group
	 * @return [true/false] {True if DONE!}
	 *
	 */
	public abstract boolean addUserToAD(String name, String pass, String samid,
			String office, String group, String lastname, String cc);

	/**
	 * Remove user or computer from AD
	 *
	 * @param name
	 * @param type
	 * @param isDsn
	 * @return
	 */
	public abstract boolean remObjectFromAD(String name, String type,
			boolean isDsn);
	
	public abstract String getIpAD();

	public abstract void setIpAD(String IPAD);

	public abstract String getUPNAD();

	public abstract void setUPNAD(String UPN);

	/**
	 * Add User cc, email.
	 * @param name
	 * @param pass
	 * @param samid
	 * @param office
	 * @param group
	 * @param lastname
	 * @param cc
	 * @param email
	 * @return
	 */
	public boolean addUserToAD(String name, String pass, String samid, String office,
			String group, String lastname, String cc, String email);

	/**
	 * Return a list with Dsn Objects, based on OU and type
	 * @param ou Organization Unit on AD for search the list of type.
	 * @param type [user, group, computer]
	 * @return
	 */
	public List<DsnObjectAd> dsnToObject(String ou, String type);
	
	public void setOUGROUPS(String ouGroups);

	public String getOUGROUPS();

	/**
	 * Return a list with Dsn Objects, based in wildcard search filter.
	 * 
	 * @param ou
	 * @param type
	 * @param filter
	 * @return
	 */
	public List<DsnObjectAd> dsnToObjectByFilter(String ou, String type, String object);
	
	/**
	 * Return list of DsnObjects, based in ou type, filter and object filters.
	 * 
	 * @param ou
	 * @param type 
	 * @param filter [samid, memberof, email, tel] / [alias, group member, mail, id /cedula]
	 * @param object [object to search valid *]
	 * @return
	 */
	public String[] dsnToListByFilter(String type,
			String filter, String object);

	public List<String> getOUNames(String UPNAD, String IPAD, String USERAD, String PASSAD);

	public List<String> getOUNames();

	public String dsnObjectGroupsByUser(String user);

	public String groupComparatorByUser(String user);

	public void setGroupNamesLocal(List<String> groups);

}