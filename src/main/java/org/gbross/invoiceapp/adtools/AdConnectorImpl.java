/**
 * 
 */
package org.gbross.invoiceapp.adtools;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.gbross.invoiceapp.model.Domain;
import org.gbross.invoiceapp.model.DsnObjectAd;
import org.gbross.invoiceapp.util.StdOutHandle;
import org.molaya.utils.RuntimeExec;

/**
 * This class connect Active Directory with Application.
 * 
 * @author molaya
 * @param
 *
 */
@Component
public class AdConnectorImpl implements AdConnector {

	private RuntimeExec runTimex;
	private String objectDsn;
	private String error;
	private String USERAD;
	private String PASSAD;
	private String IPAD;
	private String UPNAD;
	private String OUGROUPS;
	private List<String> groupNames;

	/**
	 * Constructor
	 */
	public AdConnectorImpl() {
		this.runTimex = new RuntimeExec();
		this.error = "";
		this.USERAD = "chgpass";
		this.PASSAD = "Gesti0nCOrb3";
		this.IPAD = "10.201.1.15";
		this.UPNAD = "crediorbe.local";
		this.OUGROUPS = "Perfiles_Nave";
	}

	/**
	 * Constructor with domain
	 * 
	 * @param domain
	 */
	@Autowired
	public AdConnectorImpl(Domain domain) {
		this.runTimex = new RuntimeExec();
		this.error = "";
		this.USERAD = domain.getUserAd();
		this.PASSAD = domain.getPassAd();
		this.IPAD = domain.getIpAd();
		this.UPNAD = domain.getUpnAd();
		this.OUGROUPS = domain.getOuGroups();

	}

	/**
	 * Set domain to AdController
	 */
	public void setDomain(Domain domain) {
		this.USERAD = domain.getUserAd();
		this.PASSAD = domain.getPassAd();
		this.IPAD = domain.getIpAd();
		this.UPNAD = domain.getUpnAd();
		this.OUGROUPS = domain.getOuGroups();
	}

	@Override
	public String getOUGROUPS() {
		return OUGROUPS;
	}

	@Override
	public void setOUGROUPS(String ouGroups) {
		OUGROUPS = ouGroups;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#getIpAD()
	 */
	@Override
	public String getIpAD() {
		return IPAD;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#setIpAD(java.lang.String)
	 */
	@Override
	public void setIpAD(String IPAD) {
		this.IPAD = IPAD;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#getUPNAD()
	 */
	@Override
	public String getUPNAD() {
		return UPNAD;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#setUPNAD(java.lang.String)
	 */
	@Override
	public void setUPNAD(String UPN) {
		this.UPNAD = IPAD;
	}

	/**
	 * This validate if the user exist in the AD
	 *
	 * @param user
	 * @return
	 */
	private boolean validateUser(String user) {
		return this.getObjectFromAD(user, "user");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#setPassUsrAd(java.lang.String)
	 */
	@Override
	public void setPassUsrAd(String pwdad) {
		PASSAD = pwdad;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#getObjectString()
	 */
	@Override
	public String getObjectString() {
		return objectDsn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#getErrors()
	 */
	@Override
	public String getErrors() {
		return error;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#setAdminUsrAd(java.lang.String)
	 */
	@Override
	public void setAdminUsrAd(String usrad) {
		USERAD = usrad;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#getObjectFromAD(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean getObjectFromAD(String name, String type) {
		String command = "dsquery " + type + " -samid " + name + " -s " + IPAD
				+ " -u " + USERAD + " -p " + PASSAD;
		if (type.equalsIgnoreCase("computer")) {
			command = command.replace("-samid", "-name");
		}
		StdOutHandle.info("Active Controller: " + command);
		runTimex.runProcess(command);
		objectDsn = runTimex.getMessage().replace("¡", "í").replace("¢", "ó")
				.replace("‚", "é").replace("£", "ú").replace("ÿþ", "")
				.replace("¤", "ñ");
		if (objectDsn.contains("CN=")) {
			StdOutHandle.info(objectDsn);
			return true;
		}
		error += "\n" + runTimex.getMessageError();
		StdOutHandle.error(error);
		return false;
	}

	@Override
	public List<DsnObjectAd> dsnToObject(String ou, String type) {
		String aux = "samid";
		String cn = null, ous = null, dc = null;
		String dom = UPNAD.replace(".local", "");
		String[] auxi = null;
		StdOutHandle.error("INSIDE DSNOBJECT");
		List<DsnObjectAd> users = new ArrayList<DsnObjectAd>();
		if (type.contains("computer"))
			aux = "name";
		String cmd = "dsquery " + type + " \"OU=" + ou + ", DC="
				+ UPNAD.replace(".local", "") + ", DC="
				+ UPNAD.replace(dom + ".", "") + "\" -" + aux + " * -s " + IPAD
				+ " -u " + USERAD + " -p " + PASSAD;
		StdOutHandle.error("GETTING GROUPS ON AD: " + cmd);
		runTimex.runProcess(cmd);
		String list = runTimex.getMessage().replace("¡", "í").replace("¢", "ó")
				.replace("‚", "é").replace("£", "ú").replace("ÿþ", "")
				.replace("¤", "ñ");
		if (list.isEmpty())
			return null;
		String[] result = list.split("\n");
		for (String itemr : result) {
			/*
			 * Warranty to return the last [OU] EX:
			 * "OU=Usuarios, [OU]=Office, DC=domain, DC=local" ^
			 * "[OU]=Office, DC=domain, DC=local" ^
			 */
			System.err.println("*** " + itemr + " " + cn + " " + ou + " " + dc);
			auxi = itemr.split("\"CN=")[1].split(",OU=");
			cn = itemr.split("\"CN=")[1].split(",")[0];
			ous = itemr.split("\"CN=")[1].split(",OU=")[auxi.length - 1]
					.split(",")[0];
			dc = itemr.split("\"CN=")[1].split(",OU=")[auxi.length - 1]
					.split(",DC=")[1];
			System.err.println("*** " + itemr + " " + cn + " " + ou + " " + dc);
			users.add(new DsnObjectAd(cn, ou, dc, itemr));
		}
		System.err.println("*** " + "OUT");
		return users;

	}

	@Override
	public String dsnObjectGroupsByUser(String dsn) {
		String cmd = "dsget user " + dsn + " -memberof  -s " + IPAD + " -u "
				+ USERAD + " -p " + PASSAD;
		try {
			runTimex.runProcess(cmd);

		} catch (RuntimeException ex) {
			StdOutHandle.error("MEMBEROFDSN: " + ex);
			return null;
		}
		return runTimex.getMessage();
	}

	@Override
	public String groupComparatorByUser(String dsn) {
		String cmd = "dsget user " + dsn + " -memberof  -s " + IPAD + " -u "
				+ USERAD + " -p " + PASSAD;
		try {
			runTimex.runProcess(cmd);
		} catch (RuntimeException ex) {
			StdOutHandle.error("COMPARATOR: " + ex);
			return null;
		}
		for (String group : groupNames) {
			if (runTimex.getMessage().contains(group))
				return group;
		}
		return null;

	}

	@Override
	public List<DsnObjectAd> dsnToObjectByFilter(String ou, String type,
			String object) {
		String aux = "samid";
		String cn = null, dc = null, samid = null, cc = null, email = null, ous = null;
		String dom = UPNAD.replace(".local", "");
		String[] auxi = null;
		List<DsnObjectAd> users = new ArrayList<DsnObjectAd>();
		String oufilter = "\"OU=" + ou + ", DC=" + UPNAD.replace(".local", "")
				+ ", DC=" + UPNAD.replace(dom + ".", "") + "\"";
		if (ou == null || ou == "" || ou.equals("*"))
			oufilter = "";

		if (type.contains("computer"))
			aux = "name";
		String cmd = "dsquery " + type + " " + oufilter + "  -" + aux + " "
				+ object + " -limit 1000 -s " + IPAD + " -u " + USERAD + " -p "
				+ PASSAD;
		try {
			runTimex.runProcess(cmd);
		} catch (RuntimeException ex) {
			StdOutHandle.error("DSNBYFILTER" + ex);
			return null;
		}
		String list = runTimex.getMessage().replace("ó", "ñ").replace("¡", "í")
				.replace("¢", "ó").replace("‚", "é").replace("£", "ú")
				.replace("ÿþ", "").replace("¤", "ñ").replace("�", "ó");
		if (list.isEmpty()) {
			StdOutHandle.error("NOFOUND: " + list);
			return null;
		}

		String[] result = list.split("\n");

		int i = 0;
		for (String itemr : result) {
			/*
			 * Warranty to return the last [OU] EX:
			 * "OU=Usuarios, [OU]=Office, DC=domain, DC=local" ^
			 * "[OU]=Office, DC=domain, DC=local" ^
			 */
			if (itemr.isEmpty() || itemr.length() <= 0)
				return users;
			auxi = itemr.split("\"CN=")[1].split(",OU=");
			cn = itemr.split("\"CN=")[1].split(",")[0];
			ous = itemr.split("\"CN=")[1].split(",OU=")[auxi.length - 1]
					.split(",")[0];
			dc = itemr.split("\"CN=")[1].split(",OU=")[auxi.length - 1]
					.split(",DC=")[1];

			if (!type.contains("computer")) {
				samid = dsnToListByFilter(type, "samid", itemr)[1];
				email = dsnToListByFilter(type, "email", itemr)[1];
				cc = dsnToListByFilter(type, "tel", itemr)[1];
			} else {
				samid = dsnToListByFilter(type, "samid", itemr)[1].replace("$",
						"");

			}
			users.add(new DsnObjectAd(cn, ous, dc, itemr, samid, email, cc,
					type, i++));
		}

		return users;

	}

	@Override
	public String[] dsnToListByFilter(String type, String filter, String object) {
		String aux = "samid";
		if (type == "computer")
			aux = "name";
		String cmd = "dsget " + type + " " + object + " -" + filter + " -s "
				+ IPAD + " -u " + USERAD + " -p " + PASSAD;
		runTimex.runProcess(cmd, false);
		String list = runTimex.getMessage().replace("¡", "í").replace("¢", "ó")
				.replace("‚", "é").replace("£", "ú").replace("ÿþ", "")
				.replace("¤", "ñ");
		String[] result = list.split("\n");
		return result;
	}

	@Override
	public List<String> getAllNavigationGroups(String ou) {
		List<String> result = new ArrayList<String>();
		StdOutHandle.info("ALL NAVIGATIONS");
		List<DsnObjectAd> dsnGroups = dsnToObject(OUGROUPS, "group");
		if (dsnGroups == null) {
			result.add("TIMEOUT CONNECTION");
			return result;
		}
		for (DsnObjectAd dsn : dsnGroups) {

			result.add(dsn.getName());
		}
		Collections.sort(result, (o1, o2) -> o1.compareTo(o2));
		groupNames = result;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * jar.app.ad.AdControllerInterface#getGroupOfObjectDSN(java.lang.String,
	 * java.lang.String, boolean)
	 */
	@Override
	public String getGroupOfObjectDSN(String name, String type, boolean isDsn) {

		if (type == null) {
			type = "user";
		}
		if (!isDsn) {
			name = this.getObjectDSN(name, type);
			if (name == null) {
				return null;
			}
		}
		String group = null;
		String command = "dsget user " + name + " -memberof -s " + IPAD
				+ " -u " + USERAD + " -p " + PASSAD;

		runTimex.runProcess(command);
		if (runTimex.getMessage().toLowerCase().contains("credifull")) {
			group = "CrediFull";
		} else if (runTimex.getMessage().toLowerCase().contains("credibasico")) {
			group = "CrediBasico";
		} else if (runTimex.getMessage().toLowerCase().contains("crediminimo")) {
			group = "CrediMinimo";
		} else if (runTimex.getMessage().toLowerCase().contains("usb")) {
			group = "Block_USB Policy";
		} else {
			error += "\n" + runTimex.getMessage();
		}
		return group;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#changeObjectGroup(java.lang.String,
	 * java.lang.String, java.lang.String, boolean)
	 */
	@Override
	public boolean changeObjectGroup(String newGroup, String name, String type,
			boolean isDsn) {

		String command = null;
		String oldGroup = null;

		if (!isDsn) {
			name = this.getObjectDSN(name, type);
			if (name == null) {
				return false;
			}
		}

		oldGroup = this.getGroupOfObjectDSN(name, null, true);
		if (oldGroup == null) {
			if (this.addMembersToGroup(newGroup, name, null, true)) {
				StdOutHandle.info("ADDEDED NEW GROUP: " + newGroup
						+ " to user: " + name);
				return true;
			}
		}

		if (this.deleteMembersFromGroup(oldGroup, name, null, true)) {
			StdOutHandle.info("DELETED GROUP: " + oldGroup + " to user: "
					+ name);
			if (this.addMembersToGroup(newGroup, name, null, true)) {
				StdOutHandle.info("ADDEDED NEW GROUP: " + newGroup
						+ " to user: " + name);
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#addMembersToGroup(java.lang.String,
	 * java.lang.String, java.lang.String, boolean)
	 */
	@Override
	public boolean addMembersToGroup(String group, String name, String type,
			boolean isDsn) {
		if (!isDsn) {
			name = this.getObjectDSN(name, type);
			if (name == null) {
				return false;
			}
		}
		String myGroup = this.getObjectDSN(group, "group");
		String command = "dsmod group " + myGroup + " -addmbr "
				+ name.replace("\n", "");
		command += " -s " + IPAD + " -u " + USERAD + " -p " + PASSAD;
		StdOutHandle.info("Active Controller: " + command);
		runTimex.runProcess(command);
		if (runTimex.getMessage().contains("succeeded:CN=")) {
			StdOutHandle.info("OKADDGROUP--" + runTimex.getMessage());
			return true;
		}
		error += "\n" + runTimex.getMessageError();
		StdOutHandle.error(error);
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * jar.app.ad.AdControllerInterface#deleteMembersFromGroup(java.lang.String,
	 * java.lang.String, java.lang.String, boolean)
	 */
	@Override
	public boolean deleteMembersFromGroup(String group, String name,
			String type, boolean isDsn) {
		if (!isDsn) {
			name = this.getObjectDSN(name, type);
			if (name == null) {
				return false;
			}
		}
		String myGroup = this.getObjectDSN(group, "group");
		String command = "dsmod group " + myGroup + " -rmmbr "
				+ name.replace("\n", "");
		command += " -s " + IPAD + " -u " + USERAD + " -p " + PASSAD;
		StdOutHandle.info("Active Controller: " + command);
		runTimex.runProcess(command);
		if (runTimex.getMessage().contains("succeeded:CN=")) {
			StdOutHandle.info("OKREMGROUP--" + runTimex.getMessage());
			return true;
		}
		error += "\n" + runTimex.getMessageError();
		StdOutHandle.error(error);
		return false;
	}

	@Override
	public List<String> getOUNames(String UPNAD, String IPAD, String USERAD,
			String PASSAD) {
		List<String> ous = null;
		String upn = UPNAD.replace(".local", "");
		String cn = null;
		String command = "dsquery ou DC=" + upn + ",DC="
				+ UPNAD.replace(upn + ".", "") + " -scope onelevel" + " -s "
				+ IPAD + " -u " + USERAD + " -p " + PASSAD;

		StdOutHandle.info("Active Controller: " + command);
		String list = runTimex.getMessage().replace("ó", "ñ").replace("¡", "í")
				.replace("¢", "ó").replace("‚", "é").replace("£", "ú")
				.replace("ÿþ", "").replace("¤", "ñ").replace("�", "ó");
		if (list.isEmpty()) {
			System.err.println("OUT SEARCH * " + list);
			return null;
		}

		String[] result = list.split("\n");

		int i = 0;
		for (String itemr : result) {
			/*
			 * Warranty to return the last [OU] EX:
			 * "OU=Usuarios, [OU]=Office, DC=domain, DC=local" ^
			 * "[OU]=Office, DC=domain, DC=local" ^
			 */
			if (itemr.isEmpty() || itemr.length() <= 0)
				return ous;
			cn = itemr.split("\"OU=")[1].split(",")[0];
			ous.add(cn);
			StdOutHandle.info("INSIDE GET NAMES OUS: " + cn);
		}
		error += "\n" + runTimex.getMessageError();
		StdOutHandle.error(error);
		return ous;
	}

	@Override
	public List<String> getOUNames() {
		List<String> myous = new ArrayList<String>();
		String upn = UPNAD.replace(".local", "");
		String cn = null;
		String command = "dsquery ou DC=" + upn + ",DC="
				+ UPNAD.replace(upn + ".", "") + " -scope onelevel" + " -s "
				+ IPAD + " -u " + USERAD + " -p " + PASSAD;

		StdOutHandle.info("Active Controller: " + command);
		runTimex.runProcess(command);

		String list = runTimex.getMessage().replace("ó", "ñ").replace("¡", "í")
				.replace("¢", "ó").replace("‚", "é").replace("£", "ú")
				.replace("ÿþ", "").replace("¤", "ñ").replace("�", "ó");
		if (list.isEmpty()) {
			System.err.println("OUT SEARCH * " + list);
			return null;
		}

		String[] result = list.split("\n");

		int i = 0;
		for (String itemr : result) {
			/*
			 * Warranty to return the last [OU] EX:
			 * "OU=Usuarios, [OU]=Office, DC=domain, DC=local" ^
			 * "[OU]=Office, DC=domain, DC=local" ^
			 */
			if (itemr.isEmpty() || itemr.length() <= 0)
				return null;
			// StdOutHandle.error("GETTING OU NAMES "+ itemr);
			// StdOutHandle.error("GETTING OU NAMES "+ itemr.split("\"OU=")[1]);
			// StdOutHandle.error("GETTING OU NAMES "+
			// itemr.split("\"OU=")[1].replace(",DC="+upn+",DC="+UPNAD.replace(upn+".",
			// "")+"\"",""));
			cn = itemr.split("\"OU=")[1]
					.replace(
							",DC=" + upn + ",DC="
									+ UPNAD.replace(upn + ".", "") + "\"", "");

			StdOutHandle.info("INSIDE GET NAMES OUS: " + cn);
			myous.add(cn);
		}
		error += "\n" + runTimex.getMessageError();
		StdOutHandle.error(error);
		Collections.sort(myous, (o1, o2) -> o1.compareTo(o2));
		return myous;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#getUserDSN(java.lang.String)
	 */
	@Override
	public String getUserDSN(String user) {
		if (this.getObjectFromAD(user, "user")) {
			StdOutHandle.info("Ok Searching...");
			return objectDsn;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#getObjectDSN(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String getObjectDSN(String name, String type) {
		if (this.getObjectFromAD(name, type)) {
			StdOutHandle.info("Ok Searching DSN...");
			return objectDsn;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#resetPasswordUser(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean resetPasswordUser(String user, String pass) {
		if (user == null) {
			return false;
		}
		String command = "dsmod user " + user + " -pwd " + pass;
		command += " -mustchpwd yes -u " + USERAD + " -p " + PASSAD + " -s "
				+ IPAD;

		StdOutHandle.info("Active Controller: " + command);
		runTimex.runProcess(command);
		if (runTimex.getMessage().contains("succeeded:CN=")) {
			StdOutHandle.info("OK---" + runTimex.getMessage());
			return true;
		}
		if (runTimex.getMessage().contains("Check the")) {
			StdOutHandle.info("WRONG POLICIES---" + runTimex.getMessage());
		}
		error += "\n" + runTimex.getMessage();
		StdOutHandle.error("ERROR---" + error);
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * jar.app.ad.AdControllerInterface#resetPasswordUsername(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean resetPasswordUsername(String user, String pass) {
		if (user == null) {
			return false;
		}
		String command = "dsquery user -samid " + user + " -u " + USERAD
				+ " -p " + PASSAD + " -s " + IPAD + " | dsmod -pwd " + pass;
		command += " -mustchpwd yes -u " + USERAD + " -p " + PASSAD + " -s "
				+ IPAD;

		StdOutHandle.info("Active Controller: " + command);

		runTimex.runProcess(command);
		if (runTimex.getMessage().contains("succeeded:CN=")) {
			StdOutHandle.info("OK---" + runTimex.getMessage());
			return true;
		}
		if (runTimex.getMessage().contains("Check the")) {
			StdOutHandle.info("WRONG POLICIES---" + runTimex.getMessage());
		}
		error += "\n" + runTimex.getMessage();
		StdOutHandle.error("ERROR---" + error);
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#addUserToAD(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean addUserToAD(String name, String pass, String samid,
			String office, String group, String lastname, String cc) {
		if (name == null || cc == null)
			return false;

		String command = "dsadd user \"CN=" + name + " " + lastname
				+ ",OU=Usuarios,OU=" + office;
		if (lastname.contains(" ")) {
			lastname = "\"" + lastname + "\"";
		}
		if (name.contains(" ")) {
			name = "\"" + name + "\"";
		}
		command += ",DC=crediorbe,DC=local\" -samid " + samid + " -pwd " + pass
				+ " -upn " + samid + "@" + UPNAD + " -fn " + name + " -ln "
				+ lastname + " -tel " + cc.replace(" ", "") + " -office " + cc
				+ " -memberof " + getObjectDSN(group, "group")
				+ " -mustchpwd yes -u " + USERAD + " -p " + PASSAD
				+ " -disabled no -s " + IPAD;

		StdOutHandle.info("Active Controller: " + command);

		runTimex.runProcess(command);
		if (runTimex.getMessage().contains("succeeded:CN=")) {
			StdOutHandle.info("OK---" + runTimex.getMessage());
			return true;
		}
		if (runTimex.getMessage().contains("Check the")) {
			StdOutHandle.info("WRONG INFORMATION---" + runTimex.getMessage());
		}
		error += "\n" + runTimex.getMessage();
		StdOutHandle.error("ERROR---" + error);
		return false;
	}

	@Override
	public boolean addUserToAD(String name, String pass, String samid,
			String office, String group, String lastname, String cc,
			String email) {
		if (name == null || cc == null)
			return false;

		String command = "dsadd user \"CN=" + name + " " + lastname
				+ ",OU=Usuarios,OU=" + office;
		if (lastname.contains(" ")) {
			lastname = "\"" + lastname + "\"";
		}
		if (name.contains(" ")) {
			name = "\"" + name + "\"";
		}
		command += ",DC=crediorbe,DC=local\" -samid " + samid + " -pwd " + pass
				+ " -upn " + samid + "@" + UPNAD + " -fn " + name + " -ln "
				+ lastname + " -tel " + cc.replace(" ", "") + " -email "
				+ email + " -office " + cc + " -memberof "
				+ getObjectDSN(group, "group") + " -mustchpwd yes -u " + USERAD
				+ " -p " + PASSAD + " -disabled no -s " + IPAD;

		StdOutHandle.info("Active Controller: " + command);

		runTimex.runProcess(command);
		if (runTimex.getMessage().contains("succeeded:CN=")) {
			StdOutHandle.info("OK---" + runTimex.getMessage());
			return true;
		}
		if (runTimex.getMessage().contains("Check the")) {
			StdOutHandle.info("WRONG INFORMATION---" + runTimex.getMessage());
		}
		error += "\n" + runTimex.getMessage();
		StdOutHandle.error("ERROR---" + error);
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jar.app.ad.AdControllerInterface#remObjectFromAD(java.lang.String,
	 * java.lang.String, boolean)
	 */
	@Override
	public boolean remObjectFromAD(String name, String type, boolean isDsn) {

		String command = null;
		if (!isDsn) {
			name = this.getObjectDSN(name, type);
			if (name == null) {
				return false;
			}
		}
		command = "dsrm " + name;
		command += " -noprompt -u " + USERAD + " -p " + PASSAD + " -s " + IPAD;

		StdOutHandle.info("Active Controller: " + command);

		runTimex.runProcess(command);
		if (runTimex.getMessage().contains("succeeded:CN=")) {
			StdOutHandle.info("OK---" + runTimex.getMessage());
			return true;
		}
		if (runTimex.getMessage().contains("Check the")) {
			StdOutHandle.info("WRONG POLICIES---" + runTimex.getMessage());
		}
		error += "\n" + runTimex.getMessage();
		StdOutHandle.error("ERROR AD---" + error);
		return false;
	}

	@Override
	public void setGroupNamesLocal(List<String> groups) {
		groupNames = groups;
	}

}
