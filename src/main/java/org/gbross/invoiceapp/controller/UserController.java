package org.gbross.invoiceapp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.gbross.invoiceapp.dao.DomainDAO;
import org.gbross.invoiceapp.dao.UserDAO;
import org.gbross.invoiceapp.model.HistoryMemory;
import org.gbross.invoiceapp.model.UserApp;
import org.gbross.invoiceapp.util.StdOutHandle;
import org.gbross.invoiceapp.util.UserAdConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/users")
public class UserController {
	@Autowired
	private UserDAO userdao;
	
	@Autowired
	private DomainDAO domaindao;
	
	@Autowired
	private HistoryMemory history;
	
	@RequestMapping(value = "/getList", method = RequestMethod.GET)
	@ResponseBody
	public List<UserApp> getUserList(HttpSession session, Model model) {
		System.out.println("HOLA NO SE SI SOY NULO");
		List<UserApp> users = new ArrayList<UserApp>();
		System.out.println("HOLA NO SE SI SOY NULO");
			users = userdao.listUsers();
			System.out.println("LLORE?");
		return users;
	}
//	
//	@RequestMapping(value = "/newUpdate", method = RequestMethod.POST)
//	public String addNewUser(@ModelAttribute UserApp user, HttpServletRequest request, Model model) {
//		UserAdConnector userapp = new UserAdConnector(userdao.getByUsername(request
//				.getRemoteUser()));
//		StdOutHandle.alert("Profile --- ");
//		if(user.getUsername().equalsIgnoreCase(userapp.getUser().getUsername())){
//			userapp.getUser().setName(user.getName());
//			userapp.getUser().setPassword(user.getPassword());
//			userdao.saveOrUpdate(userapp.getUser());
//		}
//		else{
//			if(userapp.getUser().getRole().matches("ROOT"))
//				userdao.saveOrUpdate(user);
//			else
//				return "redirect:/dashboard?errorupdate";
//		}
//		return "redirect:/dashboard?newupdated";
//	}
//	
//	@RequestMapping(value = "/config", method = RequestMethod.GET)
//	public String formNewUser(HttpServletRequest request, Model model) {
//		model.addAttribute("userapp", new UserApp());
//		model.addAttribute("usrslist", userdao.listUsers());
//		model.addAttribute("domslist", domaindao.getAllDomainNames());
//		model.addAttribute("readonly", false);
//		return "configuration";
//	}
//	
//	@RequestMapping(value = "/myProfile", method = RequestMethod.GET)
//	public String formProfile(HttpServletRequest request, Model model) {
//		UserAdConnector userapp = new UserAdConnector(userdao.getByUsername(request
//				.getRemoteUser()));
//		userdao.saveOrUpdate(userapp.getUser());
//		model.addAttribute("userapp", userapp.getUser());
//		model.addAttribute("usrslist", userdao.listUsers());
//		model.addAttribute("domslist", domaindao.getAllDomainNames());
//		model.addAttribute("readonly", true);
//		history.setUserMap(request.getRemoteUser(), userapp);
//		List<String> groups = userapp.getGroups();
//		history.setGroupMap(userapp.getUser().getUsername(), groups);
//		return "configuration";
//	}
//	
//	@RequestMapping(value = "/myProfile", method = RequestMethod.POST)
//	public String updateProfile(@ModelAttribute UserApp user, HttpServletRequest request, Model model) {
//		UserApp userapp = userdao.getByUsername(request.getRemoteUser());
//		userapp.setPassword(user.getPassword());
//		userapp.setName(user.getName());
//		userdao.saveOrUpdate(userapp);
//		return "redirect:/dashboard?updated";
//	}
//	
//
//	@RequestMapping(value = "/resetPass", method = RequestMethod.POST)
//	public String saveContact(@ModelAttribute UserApp user, HttpServletRequest request) {
//		StdOutHandle.error("CAMBIANDO CONTRASEÑA");
//		UserAdConnector userapp = new UserAdConnector(userdao.getByUsername(request
//					.getRemoteUser()));
//		userapp.getUser().setPassword(user.getPassword());
//		userdao.saveOrUpdate(userapp.getUser());
//		return "redirect:/dashboard?updated";
//	}
//	
	

}
