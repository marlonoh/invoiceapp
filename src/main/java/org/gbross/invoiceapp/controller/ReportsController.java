package org.gbross.invoiceapp.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.gbross.invoiceapp.dao.DomainDAO;
import org.gbross.invoiceapp.dao.ReportDAO;
import org.gbross.invoiceapp.dao.UserDAO;
import org.gbross.invoiceapp.model.Domain;
import org.gbross.invoiceapp.model.DsnObjectAd;
import org.gbross.invoiceapp.model.ReportLogs;
import org.gbross.invoiceapp.model.UserApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/reports")
public class ReportsController {
	@Autowired
	private UserDAO userdao;

	@Autowired
	private DomainDAO domaindao;

	@Autowired
	private ReportDAO reportdao;

	@RequestMapping(value = "/getUserList", method = RequestMethod.GET)
	@ResponseBody
	public List<UserApp> getUserList(HttpSession session, Model model) {
		System.out.println("HOLA NO SE SI SOY NULO");
		List<UserApp> users = new ArrayList<UserApp>();
		System.out.println("HOLA NO SE SI SOY NULO");
		users = userdao.listUsers();
		System.out.println("LLORE?");
		return users;
	}

	@RequestMapping(value = "/getDomainList", method = RequestMethod.GET)
	@ResponseBody
	public List<Domain> getDomainList(HttpSession session, Model model) {
		System.out.println("HOLA NO SE SI SOY NULO");
		List<Domain> doms = new ArrayList<Domain>();
		System.out.println("HOLA NO SE SI SOY NULO");
		doms = domaindao.getAllDomains();
		System.out.println("LLORE?");
		return doms;
	}

	@RequestMapping(value = "/getReportList", method = RequestMethod.GET)
	@ResponseBody
	public List<ReportLogs> getReportList(HttpSession session, Model model) {
		List<ReportLogs> reports = new ArrayList<ReportLogs>();
		System.out.println("HOLA NO SE SI SOY NULO");
		reports = reportdao.getReports();
		System.out.println("LLORE?");
		return reports;
	}

	@RequestMapping("")
	public String getObjects(HttpServletRequest request, Model model) {
		UserApp userapp = userdao.getByUsername(request.getRemoteUser());
		List<UserApp> users = new ArrayList<UserApp>();
		List<Domain> doms = new ArrayList<Domain>();
		List<ReportLogs> reports = new ArrayList<ReportLogs>();
		users = userdao.listUsers();
		System.out.println("REPORTS:"+users.size());
		reports = reportdao.getReports();
		doms = domaindao.getAllDomains();
		model.addAttribute("reports", reports);
		model.addAttribute("domains", doms);
		model.addAttribute("users", users);
		return "infReports";
	}

	@RequestMapping(value = "/csvFile")
	public void downloadCsv(HttpServletResponse response) throws IOException {
		String csvFileName = "LogsADtools.csv";
		List<ReportLogs> reports = new ArrayList<ReportLogs>();
		reports = reportdao.getReports();
		response.setContentType("text/csv");

		// creates mock data
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				csvFileName);
		response.setHeader(headerKey, headerValue);
		ServletOutputStream writer = response.getOutputStream();
		writer.print("Date,Module,Username,Status,Role,Detail\n");
		System.err.println("Writing FILE");
		for (ReportLogs dsn : reports) {
			writer.print(dsn.getDate() + "," + dsn.getModule() + ","
					+ dsn.getUsername() + "," + dsn.getStatus() + ","
					+ dsn.getRolename() + "," + dsn.getDetail() + "\n");

		}
		writer.flush();
		writer.close();

	}

}
