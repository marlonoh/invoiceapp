package org.gbross.invoiceapp.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.gbross.invoiceapp.adtools.AdConnector;
import org.gbross.invoiceapp.dao.DomainDAO;
import org.gbross.invoiceapp.dao.UserDAO;
import org.gbross.invoiceapp.dao.UserDAOImp;
import org.gbross.invoiceapp.model.Domain;
import org.gbross.invoiceapp.model.HistoryMemory;
import org.gbross.invoiceapp.model.UserApp;
import org.gbross.invoiceapp.util.StdOutHandle;
import org.gbross.invoiceapp.util.UserAdConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	@Autowired
	private UserDAO userdao;

	@Autowired
	private DomainDAO domaindao;

	@Autowired
	private HistoryMemory history;

	@Autowired
	private AdConnector ad;

	@RequestMapping("/dashboard")
	public String dash(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {

		model.addAttribute("userapp",
				userdao.getByUsername(request.getRemoteUser()));
		model.addAttribute("style", style);
		return "dashboard";
	}

	@RequestMapping("/base")
	public String base(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {

		model.addAttribute("userapp",
				userdao.getByUsername(request.getRemoteUser()));
		model.addAttribute("style", style);
		return "base";
	}
	
	@RequestMapping("/facturas")
	public String invoice(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {

		model.addAttribute("userapp",
				userdao.getByUsername(request.getRemoteUser()));
		model.addAttribute("style", style);
		return "invoice";
	}

	@RequestMapping("/contactus")
	public String contactus(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {
		model.addAttribute("style", style);
		return "frmIndex";
	}

	@RequestMapping("table")
	public String gettable(
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {
		model.addAttribute("style", style);
		return "tableTest";
	}

	@RequestMapping("register")
	public String getregister(
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {
		model.addAttribute("style", style);
		return "UserForm";
	}

	@RequestMapping("/aboutus")
	public String aboutus(
			HttpServletRequest request,
			@RequestParam(value = "style", required = false, defaultValue = "cyborg") String style,
			Model model) {
		UserApp userapp = userdao.getByUsername(request.getRemoteUser());
		model.addAttribute("userapp", userapp);
		return "aboutus";
	}

}
