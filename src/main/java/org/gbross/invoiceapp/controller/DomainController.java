package org.gbross.invoiceapp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.gbross.invoiceapp.dao.DomainDAO;
import org.gbross.invoiceapp.dao.UserDAO;
import org.gbross.invoiceapp.model.Domain;
import org.gbross.invoiceapp.model.UserApp;
import org.gbross.invoiceapp.util.UserAdConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/doms")
public class DomainController {
	@Autowired
	private DomainDAO domaindao;
	@Autowired
	private UserDAO userdao;

	@RequestMapping(value = "/getList", method = RequestMethod.GET)
	@ResponseBody
	public List<Domain> getDomainList(HttpSession session, Model model) {
		System.out.println("HOLA NO SE SI SOY NULO");
		List<Domain> doms = new ArrayList<Domain>();
		System.out.println("HOLA NO SE SI SOY NULO");
		doms = domaindao.getAllDomains();
		System.out.println("LLORE?");
		return doms;
	}

	@RequestMapping(value = "/getNameList", method = RequestMethod.GET)
	@ResponseBody
	public List<String> getNameList(HttpSession session, Model model) {
		System.out.println("HOLA NO SE SI SOY NULO");
		List<String> doms = domaindao.getAllDomainNames();
		System.out.println("HOLA NO SE SI SOY NULO");
		return doms;
	}

	@RequestMapping(value = "/newUpdate", method = RequestMethod.POST)
	public String addNewUser(@ModelAttribute Domain domain,
			HttpServletRequest request, Model model) {
		domaindao.saveOrUpdate(domain);
		return "redirect:/dashboard?newupdated";
	}

	@RequestMapping(value = "/updateUserDomain", method = RequestMethod.POST)
	public String updateDomain(@RequestParam("userid") int userid,
			@RequestParam("domain") String domain, HttpServletRequest request,
			Model model) {
		UserAdConnector userapp = new UserAdConnector(userdao.getUser(userid));
		userdao.setActiveDomainToUsername(userid, userapp.getDomain().getId(),
				domain);
		return "redirect:/dashboard?updated";
	}

	// @RequestMapping(value = "/resetPass", method = RequestMethod.POST)
	// public String saveContact(@ModelAttribute UserApp user,
	// HttpServletRequest request) {
	// UserApp userapp = (UserApp)request.getAttribute("userapp");
	// userdao.saveOrUpdate(user);
	// // model.addAttribute("userapp", userapp);
	// return "redirect:/dashboard?updated";
	// }

}
