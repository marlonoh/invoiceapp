package org.gbross.invoiceapp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("apijson")
public class TestRestController {

	@RequestMapping("/customer")
	public String getCustomer(){
		String json = "{'customer':[{'id':1,'customer':'NAVITRANS'},{'id':2,'customer':'BOTERO SOTO'}]}";
		return json;
	}
	
	@RequestMapping("/table")
	public String getTable() {
		String json = "[\n" + "    {\n" + "        \"id\": 0,\n"
				+ "        \"name\": \"Item 0\",\n"
				+ "        \"price\": \"$0\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 1,\n" + "        \"name\": \"Item 1\",\n"
				+ "        \"price\": \"$1\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 2,\n" + "        \"name\": \"Item 2\",\n"
				+ "        \"price\": \"$2\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 3,\n" + "        \"name\": \"Item 3\",\n"
				+ "        \"price\": \"$3\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 4,\n" + "        \"name\": \"Item 4\",\n"
				+ "        \"price\": \"$4\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 5,\n" + "        \"name\": \"Item 5\",\n"
				+ "        \"price\": \"$5\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 6,\n" + "        \"name\": \"Item 6\",\n"
				+ "        \"price\": \"$6\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 7,\n" + "        \"name\": \"Item 7\",\n"
				+ "        \"price\": \"$7\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 8,\n" + "        \"name\": \"Item 8\",\n"
				+ "        \"price\": \"$8\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 9,\n" + "        \"name\": \"Item 9\",\n"
				+ "        \"price\": \"$9\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 10,\n" + "        \"name\": \"Item 10\",\n"
				+ "        \"price\": \"$10\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 11,\n" + "        \"name\": \"Item 11\",\n"
				+ "        \"price\": \"$11\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 12,\n" + "        \"name\": \"Item 12\",\n"
				+ "        \"price\": \"$12\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 13,\n" + "        \"name\": \"Item 13\",\n"
				+ "        \"price\": \"$13\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 14,\n" + "        \"name\": \"Item 14\",\n"
				+ "        \"price\": \"$14\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 15,\n" + "        \"name\": \"Item 15\",\n"
				+ "        \"price\": \"$15\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 16,\n" + "        \"name\": \"Item 16\",\n"
				+ "        \"price\": \"$16\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 17,\n" + "        \"name\": \"Item 17\",\n"
				+ "        \"price\": \"$17\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 18,\n" + "        \"name\": \"Item 18\",\n"
				+ "        \"price\": \"$18\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 19,\n" + "        \"name\": \"Item 19\",\n"
				+ "        \"price\": \"$19\"\n" + "    },\n" + "    {\n"
				+ "        \"id\": 20,\n" + "        \"name\": \"Item 20\",\n"
				+ "        \"price\": \"$20\"\n" + "    }\n" + "]";
		return json;
	}
	
	@RequestMapping("tablePag")
	public String getPageTable(){
		String json = "{\n" +
				"    \"total\": 200,\n" +
				"    \"rows\": [\n" +
				"        {\n" +
				"            \"id\": 0,\n" +
				"            \"name\": \"Item 0\",\n" +
				"            \"price\": \"$0\"\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 1,\n" +
				"            \"name\": \"Item 1\",\n" +
				"            \"price\": \"$1\"\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 2,\n" +
				"            \"name\": \"Item 2\",\n" +
				"            \"price\": \"$2\"\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 3,\n" +
				"            \"name\": \"Item 3\",\n" +
				"            \"price\": \"$3\"\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 4,\n" +
				"            \"name\": \"Item 4\",\n" +
				"            \"price\": \"$4\"\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 5,\n" +
				"            \"name\": \"Item 5\",\n" +
				"            \"price\": \"$5\"\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 6,\n" +
				"            \"name\": \"Item 6\",\n" +
				"            \"price\": \"$6\"\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 7,\n" +
				"            \"name\": \"Item 7\",\n" +
				"            \"price\": \"$7\"\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 8,\n" +
				"            \"name\": \"Item 8\",\n" +
				"            \"price\": \"$8\"\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 9,\n" +
				"            \"name\": \"Item 9\",\n" +
				"            \"price\": \"$9\"\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 10,\n" +
				"            \"name\": \"Item 10\",\n" +
				"            \"price\": \"$10\"\n" +
				"        }\n" +
				"    ]\n" +
				"}";
		return json;
	}
	

}
