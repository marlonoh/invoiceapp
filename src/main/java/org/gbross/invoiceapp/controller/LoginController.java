package org.gbross.invoiceapp.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.gbross.invoiceapp.dao.UserDAO;
import org.gbross.invoiceapp.model.UserApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
	@Autowired
	private UserDAO userdao;
	
//	@RequestMapping(value = "/login", method = RequestMethod.GET)
//	public String getLoginAccess(){
//		return "login";
//	}
	
//	@RequestMapping(value = "/login", method = RequestMethod.POST)
//	public String getLogin(@ModelAttribute UserApp user, Model model, HttpSession session) {
//		boolean isLogged = userdao.validateLogin(user);		
//
//		if (isLogged) {
//			user = userdao.getMyUser();
//			userdao.saveSession(session.getId(), user.getId());
//			session.setAttribute("userapp", user);
//			model.addAttribute("validUser", user);
//			userdao.saveLog(user.getId(), user.getName() + " was logged in!",
//					"LOGIN", "OK");
//			session.setAttribute("userapp", user);
//			return "redirect:/dashboard";
//		}
//		return "redirect:/login?error";
//	}
//
//	@RequestMapping(value = "/logout", method = RequestMethod.POST)
//	public String getLogout(HttpSession session, Model model) {
//		UserApp user = (UserApp)session.getAttribute("userapp");
//		session.invalidate();
//		userdao.saveLog(user.getId(), user.getName() + " was logged out!",
//					"LOGOUT", "OK");
//		return "hero";
//	}
	
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String getLogout(Principal user, Model model) {
		model.addAttribute("msg", "Hi "+user.getName()+" No tiene permiso para acceder");
		return "My403";
	}
	
	
	@RequestMapping("/test")
	@Secured("TECH")
	@ResponseBody
	public String getTest(HttpSession session){
		return "My sessionId = " + session.getId();
	}
	
	@RequestMapping("/testing2")
	public String gettest2(HttpSession session, HttpServletRequest request) throws Exception{
		String username = request.getRemoteUser();
		String password = userdao.getByUsername(username).getPassword();
		
		return "hello";
	}
	

}
