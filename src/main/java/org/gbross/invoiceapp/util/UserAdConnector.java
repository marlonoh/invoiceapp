package org.gbross.invoiceapp.util;

import java.util.ArrayList;
import java.util.List;

import org.gbross.invoiceapp.adtools.AdConnector;
import org.gbross.invoiceapp.adtools.AdConnectorImpl;
import org.gbross.invoiceapp.model.Domain;
import org.gbross.invoiceapp.model.UserApp;

public class UserAdConnector {
	private UserApp user;
	private AdConnector ad;
	private Domain domain;
	private List<String> ous;
	private List<String> groups;
	
	public UserAdConnector(UserApp user) {
		super();
		StdOutHandle.info("UserAdConnector Constructor");
		this.user = user;
		StdOutHandle.info("Domain");
		this.domain = this.user.getDomain();
		StdOutHandle.info("AdConnector With Domain");
		this.ad = new AdConnectorImpl(domain);
		StdOutHandle.info("DomainGroups");
		this.groups = this.ad.getAllNavigationGroups(domain.getOuGroups());
		StdOutHandle.info("OUS");
		this.ous = this.ad.getOUNames();
		if(this.ous == null){
			this.ous= new ArrayList<String>();
			this.ous.add("TIMEOUT CONNECTION");
		}
		StdOutHandle.info("UserAdConnector Constructor END");
	}
	public UserApp getUser() {
		return user;
	}
	public void setUser(UserApp user) {
		this.user = user;
	}
	public AdConnector getAd() {
		return ad;
	}
	public void setAd(AdConnector ad) {
		this.ad = ad;
	}
	public Domain getDomain() {
		return domain;
	}
	public void setDomain(Domain dom) {
		this.domain = dom;
	}
	public List<String> getOus() {
		return ous;
	}
	public void setOus(List<String> ous) {
		this.ous = ous;
	}
	public List<String> getGroups() {
		return groups;
	}
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

}
