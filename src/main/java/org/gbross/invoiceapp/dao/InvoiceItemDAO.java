package org.gbross.invoiceapp.dao;

import java.util.List;

import org.gbross.invoiceapp.model.Domain;
import org.gbross.invoiceapp.model.Invoice;
import org.gbross.invoiceapp.model.InvoiceItem;
import org.gbross.invoiceapp.model.UserApp;
import org.springframework.stereotype.Component;

@Component
public interface InvoiceItemDAO {
		public void create(InvoiceItem items);
		public InvoiceItem read(int id);
		public List<InvoiceItem> readAll();
		public boolean isConnected();
		public void delete(int id);
		public void update(int id);
		
}

