package org.gbross.invoiceapp.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.gbross.invoiceapp.database.SqliteConnection;
import org.gbross.invoiceapp.model.Domain;
import org.gbross.invoiceapp.model.Role;
import org.gbross.invoiceapp.enums.Status;
import org.gbross.invoiceapp.model.UserApp;
import org.gbross.invoiceapp.util.StdOutHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserDAOImp implements UserDAO {

	private SqliteConnection db;
	private UserApp user;

	@Autowired
	public UserDAOImp(SqliteConnection db) {
		this.db = db;
		if (!this.db.isConnected()) {
			this.db.connect();
			StdOutHandle.add("Configuring users on database");
		}
		this.user = new UserApp();
	}

	@Override
	public void resetPass(UserApp user) {
		String sql = "UPDATE TBLUSERS SET password=\"" + user.getPassword()
				+ "\", WHERE userid=" + user.getId() + "";
		db.write(sql);
	}

	@Override
	public void saveOrUpdate(UserApp user) {
		UserApp aux = getByUsername(user.getUsername());
		StdOutHandle.error("UserApp: "+ user.toString());
		String dom = user.getMydomain();
		String sql = null;
		if (aux != null) {
			// update
			setActiveDomainToUsername(aux.getId(), aux.getDomain().getId(), user.getMydomain());
			
			sql = "UPDATE TBLUSERS SET username=\"" + user.getUsername()
					+ "\", password=\"" + user.getPassword() + "\", name=\""
					+ user.getName() + "\", email=\"" + user.getEmail()
					+ "\" " + "WHERE userid="
					+ aux.getId() + "";
			
		} else {
			// insert
			sql = "INSERT INTO TBLUSERS (username, password, name, email)"
					+ " VALUES (\"" + user.getUsername() + "\", \""
					+ user.getPassword() + "\", \"" + user.getName() + "\", \""
					+ user.getEmail() + "\" )";
			db.write(sql);
			user = getByUsername(user.getUsername());
			sql = "INSERT INTO TBLUSERS_ROL (userid, roleid) VALUES ("
					+ user.getId() + ", 1)";
			db.write(sql);
			sql = "INSERT INTO TBLUSERS_DOM (userid, domainid) VALUES ("
					+ user.getId() + ", "+getDomainByName(dom).getId()+")";
		}
		db.write(sql);
	}

	@Override
	public boolean blockUserByUsername(String username) {
		String sql = "UPDATE TBLUSERS SET status=\"DISABLED\" "
				+ " WHERE username='" + username + "'";
		db.write(sql);
		return true;
	}

	@Override
	public void delete(int userId) {
		String sql = "DELETE FROM TBLUSERS WHERE userid=" + userId;
		db.write(sql);
	}

	@Override
	public void delete(String username) {
		String sql = "DELETE FROM TBLUSERS WHERE username='" + username + "'";
		db.write(sql);
	}

	@Override
	public UserApp getUser(int userId) {
		ResultSet rs = null;
		String role = null;
		String username = null;
		List<String> domains = null;
		String sql = "SELECT * FROM TBLUSERS WHERE userid=" + userId;
		rs = db.read(sql);
		try {
			if (rs.next()) {
				username = rs.getString("username");
				return new UserApp(username, rs.getString("password"),
						rs.getString("name"), rs.getString("email"), 0,
						rs.getInt("userid"), rs.getInt("status"),
						getRoleByUsername(username),
						getDomainByUsername(username));
			}
		} catch (SQLException ex) {
			StdOutHandle.error("Error Getting User: \n" + ex);
		} catch (RuntimeException ex) {
			return null;
		}
		return null;
	}

	@Override
	public List<UserApp> listUsers() {
		List<UserApp> users = new ArrayList<UserApp>();
		String sql = "SELECT t.*, r.rolename AS role FROM TBLUSERS t "
				+ "INNER JOIN tblusers_rol tr ON tr.userid=t.userid "
				+ "INNER JOIN tblroles r ON r.roleid=tr.roleid ";
		ResultSet rs = db.read(sql);
		try {
			while (rs.next()) {
				// System.out.println(rs.getString("username")+": "+users.size());
				users.add(new UserApp(rs.getString("username"), "**********",
						rs.getString("name"), rs.getString("email"), 0, rs
								.getInt("userid"), rs.getInt("status"), rs
								.getString("role")));
			}
			users.forEach(p -> p.setDomain(getDomainByUsername(p.getUsername())));
			users.forEach(p -> System.out.println("###: " + p.getDomainName()));
			return users;
		} catch (SQLException ex) {
			StdOutHandle.error("Error Getting ListUsers: \n" + ex);
			return null;
		} catch (RuntimeException ex) {
			return null;
		}
	}

	@Override
	public ArrayList<Integer> listUserModules() {
		ArrayList<Integer> modules = new ArrayList<Integer>();
		String sql = "SELECT * FROM TBLUSERS";
		ResultSet rs = db.read(sql);

		try {
			while (rs.next())
				modules.add(rs.getInt("moduleid"));
			return modules;
		} catch (SQLException ex) {
			StdOutHandle.error("Error Getting Modules: \n" + ex);
			return null;
		} catch (RuntimeException ex) {
			return null;
		}
	}

	@Override
	public ArrayList<Role> listRoles() {
		ArrayList<Role> roles = new ArrayList<Role>();
		String sql = "SELECT * FROM TBLROLES";
		ResultSet rs = db.read(sql);

		try {
			while (rs.next())
				roles.add(new Role(rs.getInt("roleid"), rs
						.getString("rolename")));
			return roles;
		} catch (SQLException ex) {
			StdOutHandle.error("Error Getting Roles: \n" + ex);
			return null;
		} catch (RuntimeException ex) {
			return null;
		}
	}

	@Override
	public boolean validateLogin(UserApp user) {
		String sql = "SELECT * FROM TBLUSERS WHERE username='"
				+ user.getUsername() + "' and password='" + user.getPassword()
				+ "'";
		ResultSet rs = db.read(sql);
		try {
			if (rs.getString("username").equalsIgnoreCase(user.getUsername())
					&& rs.getString("password").equals(user.getPassword())) {
				if (rs.getInt("status") > Status.ENABLED.getValue())
					this.user.setStatus(rs.getInt("status"));

				this.user.setRoleid(rs.getInt("roleId"));
				this.user.setId(rs.getInt("userid"));
				this.user.setName(rs.getString("name"));
				return true;
			}
		} catch (SQLException ex) {
			System.err.println("Error Validating Login: \n" + ex);
		} catch (RuntimeException ex) {
			System.err.println("Error Validating Login: \n" + ex);
			ex.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public boolean validateModule(int modId, int roleId) {
		String sql = "SELECT * FROM TBLSESSIONS WHERE moduleid=" + modId
				+ " and roleid=" + roleId + "";
		ResultSet rs = db.read(sql);
		try {
			if (rs.next())
				return true;
		} catch (SQLException ex) {
			System.err.println("Error Validating Module: \n" + ex);

		} catch (RuntimeException ex) {
			System.err.println("Error Validating Module: \n" + ex);
			ex.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public UserApp getMyUser() {
		UserApp user = this.user;
		this.user = null;
		return user;
	}

	@Override
	public boolean isAllowed(String sessionid, int userid) {
		String sql = "SELECT * FROM TBLSESSIONS WHERE sessionid='" + sessionid
				+ "' and userid=" + userid + "";
		ResultSet rs = db.read(sql);
		try {
			if (rs.next()) {
				return true;
			}
		} catch (SQLException ex) {
			System.err.println("Error isAllowed: \n" + ex);
		} catch (RuntimeException ex) {
			System.err.println("Error isAllowed: \n" + ex);
			ex.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public boolean isConnected() {
		return db.isConnected();
	}

	@Override
	public void saveSession(String sessionid, int userid) {
		String sql = "INSERT INTO TBLSESSIONS(sessionid, userid) VALUES ('"
				+ sessionid + "'," + userid + ")";
		db.write(sql);
	}

	@Override
	public void saveLog(int userid, String info, String logname, String status) {
		String sql = "INSERT INTO TBLLOGS(userid, loginfo, logname, logstatus) VALUES ("
				+ userid
				+ ", '"
				+ info
				+ "','"
				+ logname
				+ "','"
				+ status
				+ "')";
		db.write(sql);
	}

	@Override
	public UserApp getByUsername(String username) {
		ResultSet rs = null;

		String role = getRoleByUsername(username);
		Domain doms = getDomainByUsername(username);
		if (role == null)
			role = "NONE";
		if (doms == null)
			doms = new Domain();
		String sql = "SELECT * FROM TBLUSERS WHERE username='" + username + "'";
		rs = db.read(sql);
		try {
			if (rs.next())
				return new UserApp(rs.getString("username"),
						rs.getString("password"), rs.getString("name"),
						rs.getString("email"), rs.getInt("status"), 0,
						rs.getInt("userid"), role, doms);

		} catch (SQLException ex) {
			StdOutHandle.error("Error Getting User by Username: \n" + ex);
		} catch (RuntimeException ex) {
			StdOutHandle.error("Error Getting User by Username: \n" + ex);
			return null;
		}
		StdOutHandle.error("Error Getting User by Username: \n");
		return null;
	}

	@Override
	public Domain getDomainByName(String name) {
		String sql = "SELECT * FROM TBLDOMAINS WHERE domain = '" + name + "'";
		ResultSet rs = db.read(sql);
		try {
			if (rs.next())
				return new Domain(rs.getString("domain"),
						rs.getString("username"), rs.getString("password"),
						rs.getString("ipaddress"), rs.getString("upn"),
						rs.getString("ougroup"), rs.getString("logo"),
						rs.getInt("domainid"));
		} catch (SQLException ex) {
			StdOutHandle.error("Error Getting Domain: \n" + ex);
		} catch (RuntimeException ex) {
			return null;
		}
		return null;
	}

	@Override
	public Domain getDomainById(int domainid) {
		String sql = "SELECT * FROM TBLDOMAINS WHERE domainid = '" + domainid
				+ "'";
		ResultSet rs = db.read(sql);
		try {
			if (rs.next())
				return new Domain(rs.getString("domain"),
						rs.getString("username"), rs.getString("password"),
						rs.getString("ipaddress"), rs.getString("upn"),
						rs.getString("ougroup"), rs.getString("logo"),
						rs.getInt("domainid"));
		} catch (SQLException ex) {
			StdOutHandle.error("Error Getting Domain: \n" + ex);
		} catch (RuntimeException ex) {
			return null;
		}
		return null;
	}

	@Override
	public Domain getDomainByUsername(String username) {
		ResultSet rs = null;
		String sql = "SELECT td.* FROM TBLDOMAINS td INNER JOIN TBLUSERS_DOM tud "
				+ "on tud.domainid=td.domainid INNER JOIN TBLUSERS tu "
				+ "on tud.userid=tu.userid WHERE tu.username='"
				+ username
				+ "'";

		rs = db.read(sql);
		try {
			if (rs.next()) {
				StdOutHandle.info("Correct read DomainObject by Username: "
						+ rs.getString("username"));
				return new Domain(rs.getString("domain"),
						rs.getString("username"), rs.getString("password"),
						rs.getString("ipaddress"), rs.getString("upn"),
						rs.getString("ougroup"), rs.getString("logo"),
						rs.getInt("domainid"));
			}
		} catch (SQLException ex) {
			StdOutHandle.error("Error Getting Users by domain: \n" + ex);
			return null;
		} catch (RuntimeException ex) {
			StdOutHandle.error("Error Getting Users by domain.: \n" + ex);
			return null;
		}
		return null;
	}

	@Override
	public List<Domain> getDomainsByUsername(String username) {
		ResultSet rs = null;
		List<Domain> doms = new ArrayList<Domain>();
		String sql = "SELECT td.* FROM TBLDOMAINS td INNER JOIN TBLUSERS_DOM tud "
				+ "on tud.domainid=td.domainid INNER JOIN TBLUSERS tu "
				+ "on tud.userid=tu.userid WHERE tu.username='"
				+ username
				+ "'";

		rs = db.read(sql);
		try {
			while (rs.next()) {
				StdOutHandle.info("Correct read DomainObject by Username: "
						+ rs.getString("username"));
				doms.add(new Domain(rs.getString("domain"), rs
						.getString("username"), rs.getString("password"), rs
						.getString("ipaddress"), rs.getString("upn"), rs
						.getString("ougroup"), rs.getString("logo"), rs
						.getInt("domainid")));
			}
			return doms;
		} catch (SQLException ex) {
			StdOutHandle.error("Error Getting Users by domain: \n" + ex);
		} catch (RuntimeException ex) {
			StdOutHandle.error("Error Getting Users by domain: \n" + ex);
			return null;
		}
		StdOutHandle.error("Error Getting Users by domain: \n");
		return null;
	}

	@Override
	public List<String> getDomainNamesByUsername(String username) {
		ResultSet rs = null;
		List<String> doms = new ArrayList<String>();
		String sql = "SELECT td.* FROM TBLDOMAINS td INNER JOIN TBLUSERS_DOM tud "
				+ "on tud.domainid=td.domainid INNER JOIN TBLUSERS tu "
				+ "on tud.userid=tu.userid WHERE tu.username='"
				+ username
				+ "'";

		rs = db.read(sql);
		try {
			while (rs.next()) {
				StdOutHandle.info("Correct read DomainObject by Username: "
						+ rs.getString("username"));
				doms.add(rs.getString("domain"));
			}
			return doms;
		} catch (SQLException ex) {
			StdOutHandle.error("Error Getting Users by domain: \n" + ex);
		} catch (RuntimeException ex) {
			return null;
		}
		return null;
	}

	@Override
	public void setDomainToUsername(int userid, String domain) {
		int domainid = getDomainByName(domain).getId();
		String sql = "INSERT INTO TBLUSERS_DOM (userid, domainid) VALUES("
				+ userid + ", " + domainid + ")";
		StdOutHandle.info("Adding domain to user " + sql);
		db.write(sql);
	}

	@Override
	public void setActiveDomainToUsername(int userid, int domainid,
			String newDomain) {
		int newDomainId = getDomainByName(newDomain).getId();

		String sql = "UPDATE TBLUSERS_DOM SET domainid=" + newDomainId
				+ " WHERE userid=" + userid + " and domainid=" + domainid + "";
		StdOutHandle.info("Modifying domain to user " + sql + " " + newDomain);
		db.write(sql);
	}

	@Override
	public String getRoleByUsername(String username) {
		ResultSet rs = null;
		String sql = "SELECT r.rolename FROM TBLROLES r INNER JOIN TBLUSERS_ROL tur "
				+ "on tur.roleid=r.roleid INNER JOIN TBLUSERS tu "
				+ "on tu.userid=tur.userid WHERE tu.username='"
				+ username
				+ "'";

		rs = db.read(sql);
		try {
			if (rs.next()) {
				StdOutHandle.info("Correct read role: "
						+ rs.getString("rolename"));
				return rs.getString("rolename");
			}
		} catch (SQLException ex) {
			StdOutHandle.error("Error Getting ROle: \n" + ex);
			return null;
		} catch (RuntimeException ex) {
			StdOutHandle.error("Error Getting Role.: \n" + ex);
			return null;
		}
		return null;
	}

}
