package org.gbross.invoiceapp.dao;

import java.util.Date;
import java.util.List;

import org.gbross.invoiceapp.model.ReportLogs;
import org.springframework.stereotype.Component;

@Component
public interface ReportDAO {
	public List<ReportLogs> getReports();
	public List<ReportLogs> getReportsBySamidAD(String samid);
	public List<ReportLogs> getReportsByUsername(String username);
	public List<ReportLogs> getReportsByDate(Date start, Date end);
}

