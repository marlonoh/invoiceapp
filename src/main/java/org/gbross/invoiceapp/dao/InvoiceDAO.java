package org.gbross.invoiceapp.dao;

import java.util.List;

import org.gbross.invoiceapp.model.Domain;
import org.gbross.invoiceapp.model.Invoice;
import org.gbross.invoiceapp.model.InvoiceItem;
import org.gbross.invoiceapp.model.UserApp;
import org.springframework.stereotype.Component;

@Component
public interface InvoiceDAO {
		public void create(Invoice invoice);
		public Invoice read(int reference);
		public List<Invoice> readAll();
		public List<InvoiceItem> readAllItems(int reference);
		public boolean isConnected();
		public void delete(int reference);
		public void update(Invoice invoice);
		
}

