package org.gbross.invoiceapp.dao;

import java.util.ArrayList;
import java.util.List;

import org.gbross.invoiceapp.model.Domain;
import org.gbross.invoiceapp.model.Role;
import org.gbross.invoiceapp.model.UserApp;
import org.springframework.stereotype.Component;

/**
 * @author molaya
 *
 */
@Component
public interface UserDAO {
	
	public void saveOrUpdate(UserApp user);
	
	public void delete(int userId);
	
	public UserApp getUser(int userId);
	
	public List<UserApp> listUsers();
	
	public ArrayList<Integer> listUserModules();

	public boolean validateLogin(UserApp user);
	
	public boolean validateModule(int modId, int roleId);

	public UserApp getMyUser();

	public void saveSession(String sessionid, int userid);

	public void saveLog(int userid, String info, String logname, String status);

	public boolean isConnected();

	public boolean isAllowed(String sessionid, int userid);

	public UserApp getByUsername(String username);
	
	public String getRoleByUsername(String username);

	public ArrayList<Role> listRoles();

	public boolean blockUserByUsername(String username);

	public void delete(String username);

	public void resetPass(UserApp user);

	public Domain getDomainByUsername(String username);

	public List<String> getDomainNamesByUsername(String username);

	public Domain getDomainByName(String name);

	public void setDomainToUsername(int userid, String domain);

	public Domain getDomainById(int domainid);

	public List<Domain> getDomainsByUsername(String username);

	public void setActiveDomainToUsername(int userid, int domainid, String newDomain);

}
