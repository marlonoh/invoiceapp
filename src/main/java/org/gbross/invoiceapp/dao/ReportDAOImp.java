package org.gbross.invoiceapp.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.gbross.invoiceapp.database.SqliteConnection;
import org.gbross.invoiceapp.model.ReportLogs;
import org.gbross.invoiceapp.model.UserApp;
import org.gbross.invoiceapp.util.StdOutHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReportDAOImp implements ReportDAO {
	private SqliteConnection db;
	private ReportLogs report;

	@Autowired
	public ReportDAOImp(SqliteConnection db) {
		this.db = db;
		if (!this.db.isConnected()) {
			this.db.connect();
			StdOutHandle.info("REPORTDAO DB CONNECTED!!");
		}
		this.report = new ReportLogs();
	}

	@Override
	public List<ReportLogs> getReports() {
		List<ReportLogs> reports = new ArrayList<ReportLogs>();
		ResultSet rs = null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String sql = "SELECT t.logdate AS date, u.username AS username, r.rolename AS rolename, "
				+ "t.logname AS module, t.loginfo AS detail, t.logstatus AS status  "
				+ "FROM TBLLOGS t INNER JOIN TBLUSERS u ON u.userid=t.userid INNER "
				+ "JOIN tblusers_rol ur ON ur.userid=u.userid INNER JOIN tblroles  r ON r.roleid=ur.roleid "
				+ "ORDER BY t.logdate ASC";
		rs = db.read(sql);
		try {
			while (rs.next()) {
				reports.add(new ReportLogs(df.parse(rs.getString("date")), rs
						.getString("username"), rs.getString("rolename"), rs
						.getString("module"), rs.getString("status"), rs
						.getString("detail")));
			}
			return reports;
		} catch (SQLException ex) {
			StdOutHandle.error("Error Getting Report: \n" + ex);
		} catch (RuntimeException ex) {
			return null;
		}catch (ParseException ex) {
			return null;
		}
		return null;

	}

	@Override
	public List<ReportLogs> getReportsBySamidAD(String samid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ReportLogs> getReportsByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ReportLogs> getReportsByDate(Date start, Date end) {
		// TODO Auto-generated method stub
		return null;
	}

}
