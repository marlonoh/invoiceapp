package org.gbross.invoiceapp.dao;

import java.util.List;

import org.gbross.invoiceapp.model.Domain;
import org.gbross.invoiceapp.model.UserApp;
import org.springframework.stereotype.Component;

@Component
public interface DomainDAO {
		public void saveOrUpdate(Domain dom);
		public Domain getDomain(String name);
		public List<Domain> getAllDomains();
		public List<String> getAllDomainNames();
		public boolean isConnected();
		public void delete(String name);
		public void resetCredentials(Domain domain);
		public List<String> getUsernameByDomain(String domain);
		public boolean isUsernameOnDomain(String domain, String username);
}

