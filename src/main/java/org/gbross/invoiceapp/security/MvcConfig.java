package org.gbross.invoiceapp.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("index");
		registry.addViewController("/home").setViewName("infReports");
//		registry.addViewController("/aboutus").setViewName("aboutus");
//		registry.addViewController("/contactus").setViewName("contactus");
		registry.addViewController("/contact").setViewName("contact");
//		registry.addViewController("/hello").setViewName("hello");
		registry.addViewController("/login").setViewName("login");
//		registry.addViewController("/dashboard").setViewName("dash");
//		registry.addViewController("/ad/status").setViewName("statusAction");

		org.gbross.invoiceapp.model.Banner.print();

	}

	// @Override
	// public void addResourceHandlers(ResourceHandlerRegistry registry) {
	// super.addResourceHandlers(registry);
	// registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	// registry.addResourceHandler("/static/**").addResourceLocations("/resources/static/");
	//
	// }

}