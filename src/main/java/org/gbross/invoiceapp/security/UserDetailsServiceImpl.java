package org.gbross.invoiceapp.security;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gbross.invoiceapp.dao.UserDAO;
import org.gbross.invoiceapp.database.SqliteConnection;
import org.gbross.invoiceapp.model.DataBase;
import org.gbross.invoiceapp.model.Role;
import org.gbross.invoiceapp.model.UserApp;
import org.gbross.invoiceapp.util.StdOutHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	private UserDAO dao;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		List<GrantedAuthority> authList = null;
		GrantedAuthority authority = null;
		UserDetails userDetails = null;
		UserApp userInfo = null;
		String role = "TECH";
		if (dao != null) {
			userInfo = dao.getByUsername(username);
			StdOutHandle.info("UserDetailsService : " + userInfo.getName() + " as "
					+ role + " Password:" + userInfo.getPassword());
			role = dao.getRoleByUsername(username);
			authority = new SimpleGrantedAuthority(role);
		} else {
			StdOutHandle.error("ERROR DAO");
			throw new UsernameNotFoundException("Error null username");
		}
		
		userDetails = (UserDetails) new User(userInfo.getUsername(),
				userInfo.getPassword(), true, true, true, true,
				AuthorityUtils.createAuthorityList(role));
			return userDetails;
	}

	public ArrayList<Role> getALLRoles() {
		StdOutHandle.info("Getting all roles UDS");
		return dao.listRoles();
	}

}