/**
 * 
 */
package org.gbross.invoiceapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;

/**
 * @author molaya
 *
 */
@ComponentScan
@EnableAutoConfiguration
public class AppMain {

	/**
	 * Main Class of program
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(AppMain.class, args);

	}

}
