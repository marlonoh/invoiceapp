package org.gbross.invoiceapp.enums;

public enum Status {
	ENABLED(0), BLOCKED(10), TEMPORALLY(20);
    private final int value;
    private Status(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
