package org.gbross.invoiceapp.enums;

public enum Module {
	SEARCHUSER(0), ADDUSER(1), EDITPROFILE(2), DELETEUSER(3), RESETPASSWORD(4);
    private final int value;
    private Module(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}
